#!/bin/bash

$(which php) -d memory_limit=-1 $(which composer) install --dev
$(which php) -d memory_limit=-1 $(which composer) require humbug/php-scoper composer/semver:1.7.2 --dev
git checkout composer.json
docker run -ti -d -e MYSQL_ROOT_PASSWORD=root -p 3306:3306 mariadb:latest

echo Please add "127.0.0.1 mysql" to your /etc/hosts file
