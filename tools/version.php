#!/usr/bin/php
<?php

use Composer\Semver\VersionParser;
use Humbug\PhpScoper\PhpParser\NodeVisitor\NamespaceStmt\NamespaceManipulator;
use Humbug\PhpScoper\PhpParser\NodeVisitor\NamespaceStmt\NamespaceStmtCollection;
use PhpParser\Node\Stmt\TraitUse;
use PhpParser\Node\Stmt\Namespace_;
use PhpParser\NodeTraverser;
use PhpParser\NodeVisitorAbstract;
use PhpParser\ParserFactory;
use Symfony\Component\Console\Input\ArrayInput;
use PhpParser\Node\Name;
use PhpParser\Node;
use PhpParser\PrettyPrinter\Standard;
use function Humbug\PhpScoper\create_application;

require_once __DIR__ . '/../vendor/autoload.php';

chdir( __DIR__ . '/../' );


$semver  = new VersionParser();
$version = $semver->normalize( $argv[1] );
$version = str_replace( '.', "_", $version );
global $namespace_prefix;
$namespace_prefix = "ComposePress\Core\\v{$version}";
$input            = new ArrayInput( [
	'command' => 'add-prefix',
	'-p'      => $namespace_prefix,
	'-f'      => true,
] );

$app = create_application();
$app->setAutoExit( false );
$app->run( $input );


$parser = ( new ParserFactory() )->create( ParserFactory::PREFER_PHP7 );

foreach ( [ 'BaseObject', 'Component' ] as $class ) {
	$file = __DIR__ . '/../build/Abstracts/' . $class . '.php';
	try {
		$ast = $parser->parse( file_get_contents( $file ) );
	} catch ( Error $error ) {
		echo "Parse error: {$error->getMessage()}\n";

		return;
	}

	$traverser = new NodeTraverser();
	$traverser->addVisitor( new class extends NodeVisitorAbstract {
		public function enterNode( Node $node ) {
			if ( $node instanceof TraitUse ) {
				$node->traits = array_map( function ( Name $trait ) use ( $node ) {
					/** @var string $namespace_prefix */
					global $namespace_prefix;

					return Name\FullyQualified::concat( $namespace_prefix, $trait );
				}, $node->traits );

				return $node;
			}
		}
	} );

	$ast = $traverser->traverse( $ast );

	$printer = new Standard();
	file_put_contents( $file, $printer->prettyPrintFile( $ast ) );
}
