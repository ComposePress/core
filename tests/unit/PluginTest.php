<?php

class PluginTest extends \Codeception\TestCase\WPTestCase {

	public function test_get_slug() {
		$this->assertEquals( 'test-plugin', test_plugin()->get_slug() );
	}

	public function test_get_safe_slug() {
		$this->assertEquals( 'test_plugin', test_plugin()->get_safe_slug() );
	}

	public function test_get_version() {
		$this->assertEquals( '0.1.0', test_plugin()->get_version() );
	}

	public function test_get_plugin_file() {
		$this->assertTrue( is_string( test_plugin()->get_plugin_file() ) );
	}

	public function test_get_container() {
		$this->assertInstanceOf( '\ComposePress\Dice\Dice', test_plugin()->get_container() );
	}

	public function test_get_dependencies_exist() {
		$this->assertTrue( test_plugin()->get_dependencies_exist() );
	}

	public function test_init() {
		$this->assertTrue( test_plugin()->init() );
	}

	public function test_get_asset_url() {
		$this->assertEquals( 'http://example.org/wp-content/plugins/test-plugin/test.js', test_plugin()->get_asset_url( 'test.js' ) );
	}

	public function test_get_asset_url_file() {
		$this->assertEquals( 'http://example.org/wp-content/plugins/test-plugin/test-plugin.php', test_plugin()->get_asset_url( realpath( __DIR__ . '/../test-plugin.php' ) ) );
	}

	public function test_get_asset_file() {
		$this->assertEquals( dirname( test_plugin()->get_plugin_file() ).'/assets/js/test.js', test_plugin()->get_asset_path( 'assets/js/test.js' ) );
	}

	public function test_get_plugin_dir() {
		$this->assertEquals( dirname( test_plugin()->get_plugin_file() ), test_plugin()->get_plugin_dir() );
	}
	/**
	 * @throws \Exception
	 */
	public function test_init_fail() {
		$this->assertFalse( $this->make( 'PluginMock', [ 'get_dependencies_exist' => false ] )->init() );
	}

	public function test_get_plugin_info() {
		$this->assertTrue( is_array( test_plugin()->get_plugin_info() ) );
	}

	public function test_get_plugin_info_name() {
		$this->assertEquals( 'Test plugin', test_plugin()->get_plugin_info( 'Name' ) );
	}

	/**
	 * @throws \Exception\ContainerInvalid
	 * @throws \Exception\ContainerNotExists
	 */
	public function test_container_bad_slug() {
		$this->expectException( '\Exception\ContainerNotExists' );
		new PluginMockBadSlug();
	}

	/**
	 * @throws \Exception\ContainerInvalid
	 * @throws \Exception\ContainerNotExists
	 */
	public function test_container_bad_container() {
		$this->expectException( '\Exception\ContainerInvalid' );
		new PluginMockBadContainer();
	}

	/**
	 * @throws \Exception\ContainerInvalid
	 * @throws \Exception\ContainerNotExists
	 */
	public function test_container_namespace() {
		new PluginMockNamepaceContainer();
	}

	/**
	 * @throws \Exception\ContainerInvalid
	 * @throws \Exception\ContainerNotExists
	 */
	public function test_container_namespace_bad_beginning() {
		$this->expectException( '\Exception\ContainerNotExists' );
		new PluginMockBadBeginningNamepaceContainer();
	}

	/**
	 * @throws \Exception\ContainerInvalid
	 * @throws \Exception\ContainerNotExists
	 */
	public function test_container_namespace_bad_end() {
		$this->expectException( '\Exception\ContainerNotExists' );
		new PluginMockBadEndNamepaceContainer();
	}

	/**
	 * @throws \Exception\ComponentInitFailure
	 * @throws \Exception\ContainerInvalid
	 * @throws \Exception\ContainerNotExists
	 * @throws \ReflectionException
	 */
	public function test_setup_fail() {

		$this->expectException( '\Exception\ComponentInitFailure' );

		$this->assertFalse( ( new PluginMockBadSetup() )->init() );
	}
}
