<?php

use Codeception\TestCase\WPTestCase;

class ComponentTest extends WPTestCase {

	public function test_child_component_null() {
		if ( method_exists( $this, 'expectException' ) ) {
			$this->expectException( '\Exception' );
		} else {
			$this->setExpectedException( '\Exception' );
		}
		$component = $this->get_component_mock();
		$this->assertNull( $component->child->parent );
		$this->assertInstanceOf( '\Abstracts\Component', $component->child->plugin );
	}

	private function get_component_mock() {
		return new ComponentMock( new ComponentChildMock(), new ComponentChildMock() );
	}

	/**
	 * @throws \Exception\ComponentInitFailure
	 * @throws \ReflectionException
	 */
	public function test_child_component() {
		$component = $this->get_component_mock();
		$component->init();
		$this->assertInstanceOf( '\Abstracts\Component', $component->child->parent );
		$component->child->parent = test_plugin();
		$this->assertInstanceOf( '\Abstracts\Component', $component->child->plugin );
	}

	public function test_component_array_children() {
		$component = new ComponentChildrenArrayMock();
		$component->init();
		$this->assertEquals( 2, count( $component->children ) );
		foreach ( $component->children as $child ) {
			$this->assertInstanceOf( '\Abstracts\Component', $child );
			$this->assertInstanceOf( '\Abstracts\Component', $child->parent );
			$child->parent = test_plugin();
			$this->assertInstanceOf( '\Abstracts\Component', $child->plugin );

		}
	}

	public function test_child_component_no_getter() {
		$component = $this->get_component_mock();
		$component->init();
		$this->assertFalse( $component->child2 );
	}

	/**
	 * @throws \ReflectionException
	 */
	public function test_child_component_fail() {
		$component = new ComponentFailMock();
		$this->assertInstanceOf( '\Exception\ComponentInitFailure', $component->init() );
	}

	/**
	 * @throws \ReflectionException
	 */
	public function test_child_component_exception_fail() {
		$component = new ComponentFailExceptionChildrenMock();
		$this->assertInstanceOf( '\Exception\ComponentInitFailure', $component->init() );
	}

	/**
	 * @throws \ReflectionException
	 */
	public function test_child_component_wp_error_fail() {
		$component = new ComponentFailWpErrorChildrenMock();
		$this->assertInstanceOf( '\Exception\ComponentInitFailure', $component->init() );
	}

	/**
	 * @throws \ReflectionException
	 */
	public function test_component_exception_fail() {
		$component = new ComponentFailExceptionMock();
		$this->assertInstanceOf( '\Exception\ComponentInitFailure', $component->init() );
	}

	/**
	 * @throws \ReflectionException
	 */
	public function test_component_wp_error_fail() {
		$component = new ComponentFailWpErrorMock();
		$this->assertInstanceOf( '\WP_Error', $component->init() );
	}

	/**
	 * @throws \ReflectionException
	 */
	public function test_component_load_fail() {
		$component = new ComponentFailLoadMock();
		$this->assertInstanceOf( '\Exception\ComponentMissing', $component->init() );
	}


	public function test_child_component_set_parent() {
		$component_child = new ComponentChildMock();
		$component       = $this->get_component_mock();
		$component_child->set_parent( $component );

		$this->assertEquals( $component, $component_child->get_parent() );
	}

	/**
	 * @throws \ReflectionException
	 */
	public function test_is_component_false() {
		$component = $this->get_component_mock();
		$this->assertFalse( $component->is_component( false ) );
	}

	/**
	 * @throws \ReflectionException
	 */
	public function test_is_component_null() {
		$component = $this->get_component_mock();
		$this->assertFalse( $component->is_component( null ) );
	}

	/**
	 * @throws \ReflectionException
	 */
	public function test_is_component_number() {
		$component = $this->get_component_mock();
		$this->assertFalse( $component->is_component( 1 ) );
	}

	/**
	 * @throws \ReflectionException
	 */
	public function test_is_component_stdclass() {
		$component = $this->get_component_mock();
		$this->assertFalse( $component->is_component( new stdClass() ) );
	}

	public function test_is_component_cached() {
		$component = $this->get_component_mock();
		$this->assertTrue( $component->is_component( $component ) );
		$this->assertTrue( $component->is_component( $component ) );
	}

	/**
	 * @throws \Exception
	 */
	public function test_load() {
		$component = $this->get_component_mock();
		$component->set_parent( test_plugin() );
		$this->assertTrue( $component->load( 'lazy_component' ) );
	}

	/**
	 * @throws \Exception
	 */
	public function test_load_invalid_property() {
		$component = $this->get_component_mock();
		$this->assertFalse( $component->load( 'fail' ) );
	}

	/**
	 * @throws \Exception
	 */
	public function test_load_invalid_type() {
		$component = $this->get_component_mock();
		$this->assertFalse( $component->load( 'lazy_component_bad' ) );
	}

	public function test_load_many() {
		$component = $this->get_component_mock();
		$component->set_parent( test_plugin() );
		$this->assertTrue( $component->load( 'lazy_component_many' ) );
		$this->assertTrue( $component->init() );
		$this->assertTrue( is_array( $component->lazy_component_many ) );
		$this->assertTrue( count( array_filter( array_map( function ( $component ) {
				return $component->init();
			}, $component->lazy_component_many ) ) ) > 1 );
	}

	public function test_load_many_bad() {
		$component = $this->get_component_mock();
		$component->set_parent( test_plugin() );
		$this->assertFalse( $component->load( 'lazy_component_many_bad' ) );
	}

	public function test_load_many_bad_class() {
		$this->expectException( '\Exception\ComponentMissing' );

		$component = $this->get_component_mock();
		$component->set_parent( test_plugin() );
		$component->load( 'lazy_component_many_bad_class' );
	}

	/**
	 * @throws \Exception
	 */
	public function test_load_invalid_class() {
		$component = $this->get_component_mock();
		$this->expectException( '\Exception\ComponentMissing' );
		$component->load( 'lazy_component_bad_class' );
	}

	public function test_is_loaded() {
		$component = $this->get_component_mock();
		$component->set_parent( test_plugin() );
		$component->load( 'lazy_component' );
		$this->assertTrue( $component->is_loaded( 'lazy_component' ) );
	}

	public function test_is_loaded_invalid_property() {
		$component = $this->get_component_mock();
		$this->assertFalse( $component->is_loaded( 'fail' ) );
	}

	public function test_is_loaded_stdClass() {
		$component = $this->get_component_mock();
		$this->assertFalse( $component->is_loaded( 'lazy_component_bad_stdclass' ) );
	}

	public function test_is_loaded_invalid_type() {
		$component = $this->get_component_mock();
		$this->assertFalse( $component->is_loaded( 'lazy_component_bad' ) );
	}

	public function test_is_loaded_invalid_type_many() {
		$component = $this->get_component_mock();
		$this->assertFalse( $component->is_loaded( 'lazy_component_many_bad' ) );
		$this->assertFalse( $component->is_loaded( 'lazy_component_many_bad_empty' ) );
	}

	public function test_get_plugin() {
		$component = $this->get_component_mock();
		$component->set_parent( test_plugin() );
		$this->assertEquals( test_plugin(), $component->plugin );
	}

	public function test_get_plugin_not_set() {
		$component = $this->get_component_mock();
		$this->expectException( '\Exception\Plugin' );
		$component->plugin;
	}

	public function test_get_plugin_tree_broken() {
		$component_child = new ComponentChildMock();
		$component       = $this->get_component_mock();
		$component_child->set_parent( $component );
		$this->expectException( '\Exception\Plugin' );
		$component_child->plugin;
	}

	public function test_get_closest() {
		$component_child = new ComponentChildMock();
		$component       = $this->get_component_mock();
		$component_child->set_parent( $component );
		$component->set_parent( test_plugin() );
		$this->assertInstanceOf( get_class( test_plugin() ), $component_child->get_closest( '\PluginMock' ) );
	}

	public function test_get_closest_fail() {
		$component_child = new ComponentChildMock();
		$component       = $this->get_component_mock();
		$component_child->set_parent( $component );
		$component->set_parent( test_plugin() );
		$this->assertFalse( $component_child->get_closest( '\blah' ) );
	}

	public function test_create_component() {
		$component         = $this->get_component_mock();
		$component->parent = test_plugin();
		$component->init();

		$this->assertInstanceOf( '\Abstracts\Component', $component->create_component( '\ComponentChildMock', 'test' ) );
	}

	public function test_create_component_no_args() {
		$component         = $this->get_component_mock();
		$component->parent = test_plugin();
		$component->init();

		$this->assertInstanceOf( '\Abstracts\Component', $component->create_component( '\ComponentChildMock' ) );
	}

	public function test_create_component_var_args() {
		$component         = $this->get_component_mock();
		$component->parent = test_plugin();
		$component->init();

		$this->assertInstanceOf( '\Abstracts\Component', $component->create_component( '\ComponentChildMock', 'test', 'a', 'b', 'c', 1, 2, [ 3 ] ) );
	}

	public function test_create_component_component_args() {
		$component         = $this->get_component_mock();
		$component->parent = test_plugin();
		$component->init();

		$child  = $component->create_component( '\ComponentChildMock' );
		$child2 = $component->create_component( '\ComponentChildMock' );

		$child_child = $component->create_component( '\ComponentChildChildMock', $child, $child2 );

		$this->assertInstanceOf( '\ComponentChildMock', $child_child->child );
		$this->assertInstanceOf( '\ComponentChildMock', $child_child->child2 );
	}

	public function test_is_component_camel() {
		$component = new ComponentMockCamel( new ComponentChildMock(), new ComponentChildMock() );
		$this->assertTrue( $component->is_component( 'child' ) );
		$this->assertTrue( $component->is_component( 'child2' ) );
	}

	public function test_get_components_camel() {
		$component = new ComponentMockCamel( new ComponentChildMock(), new ComponentChildMock() );
		$this->assertNotEmpty( $component->get_components() );
	}

	/**
	 * @throws \ReflectionException
	 */
	public function test_child_component_fail_inited() {
		$component = new ComponentFailMock();
		$this->assertInstanceOf( '\Exception\ComponentInitFailure', $component->init() );
		try {
			$component = new ComponentFailMock();
		} catch ( \Exception $e ) {
			$this->assertFalse( $component->get_inited() );
		}
	}

	public function test_is_component_inited() {
		$component = $this->get_component_mock();
		$component->init();
		$this->assertTrue( $component->is_inited() );
	}

	public function test_converted_component() {
		$component = new ConvertedComponent();
		$component->set_parent( test_plugin() );
		$component->init();
	}

	public function test_init_private_component_no_getter() {
		$component = new ComponentReflectionMock(new ComponentChildMock());
		$component->set_parent( test_plugin() );
		$component->init();
		$this->assertTrue( $component->test()->is_inited() );
	}
	public function test_no_auto_init(){
		$component = new ComponentNoAutoInitMock(new ComponentChildMock(),new ComponentChildMock());
		$component->set_parent( test_plugin() );
		$component->set_auto_init( false);
		$component->init();
		$this->assertFalse( $component->child->is_inited() );
		$this->assertFalse( $component->child2->is_inited() );
	}

	public function test_limit_auto_init(){
		$component = new ComponentNoAutoInitMock(new ComponentChildMock(),new ComponentChildMock());
		$component->set_parent( test_plugin() );
		$component->init();
		$this->assertFalse( $component->child->is_inited() );
		$this->assertTrue( $component->child2->is_inited() );
	}
}
