<?php


use Abstracts\Component;

class ComponentFailWpErrorChildrenMock extends Component {

	private $child;
	private $child2;

	public function __construct() {
		$this->child  = new ComponentChildFailWpErrorMock();
		$this->child2 = new ComponentChildFailWpErrorMock();
	}

	/**
	 * @return \ComponentChildMock
	 */
	public function get_child() {
		return $this->child;
	}
}
