<?php


use Abstracts\Manager;

class ManagerMock extends Manager {
	protected $modules = [
		'ComponentMock',
		'\ComponentChildMock',
	];
}
