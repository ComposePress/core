<?php


use Abstracts\Component;

class ComponentFailExceptionChildrenMock extends Component {

	private $child;
	private $child2;

	public function __construct() {
		$this->child  = new ComponentChildFailExceptionMock();
		$this->child2 = new ComponentChildFailExceptionMock();
	}

	/**
	 * @return \ComponentChildMock
	 */
	public function get_child() {
		return $this->child;
	}
}
