<?php


use Abstracts\Component;

class ComponentMockCamel extends Component {

	private $child;
	private $child2;

	public function __construct( ComponentChildMock $child, ComponentChildMock $child2 ) {
		$this->child  = $child;
		$this->child2 = $child2;
	}

	/**
	 * @return \ComponentChildMock
	 */
	public function getChild() {
		return $this->child;
	}

	/**
	 * @return \ComponentChildMock
	 */
	public function getChild2() {
		return $this->child2;
	}

	public function is_component( $component, $use_cache = true ) {
		return parent::is_component( $component, $use_cache );
	}

	public function load( $component, ...$args ) {
		return parent::load( $component, ...$args );
	}

	public function is_loaded( $component ) {
		return parent::is_loaded( $component );
	}

	public function get_components() {
		return parent::get_components();
	}

}
