<?php


use Abstracts\Component;

class ComponentChildrenArrayMock extends Component {

	private $children = [];

	public function __construct() {
		$this->children = [ new ComponentChildFailExceptionMock(), new ComponentChildFailExceptionMock() ];
	}

	/**
	 * @return array
	 */
	public function get_children() {
		return $this->children;
	}
}
