<?php


use Abstracts\Component;

class ComponentFailLoadMock extends Component {

	private $child = 'fakeclass';

	/**
	 * @return \fakeclass
	 */
	public function get_child() {
		return $this->child;
	}

	protected function load_components() {
		$this->load( 'child' );
	}
}
