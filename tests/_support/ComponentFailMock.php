<?php


use Abstracts\Component;

class ComponentFailMock extends Component {

	private $child;
	private $child2;

	public function __construct() {
		$this->child  = new ComponentChildFailMock();
		$this->child2 = new ComponentChildFailMock();
	}

	/**
	 * @return \ComponentChildMock
	 */
	public function get_child() {
		return $this->child;
	}
}
