<?php

use Abstracts\Component;

/**
 * Class ComponentChildFailMock
 */
class ComponentChildFailMock extends Component {

	/**
	 * @return bool
	 */
	public function setup() {
		return false;
	}
}
