<?php

use Abstracts\Component;

/**
 * Class ComponentChildChildMock
 */
class ComponentChildChildMock extends Component {

	private $child;
	private $child2;

	/**
	 * ComponentChildChildMock constructor.
	 *
	 * @param \ComponentChildMock      $child
	 * @param \ComponentChildChildMock $child2
	 */
	public function __construct( ComponentChildMock $child, ComponentChildMock $child2 ) {
		$this->child = $child;
		$this->child2 = $child2;
	}

	/**
	 * @return \ComponentChildMock
	 */
	public function get_child() {
		return $this->child;
	}

	/**
	 * @return \ComponentChildChildMock
	 */
	public function get_child2() {
		return $this->child2;
	}

}
