<?php


use Abstracts\Component;
use Exception\ComponentInitFailure;

class ComponentFailExceptionMock extends Component {
	/**
	 * @return \Exception\ComponentInitFailure
	 */
	public function setup() {
		return new ComponentInitFailure( 'fail' );
	}
}
