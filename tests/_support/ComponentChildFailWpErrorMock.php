<?php

use Abstracts\Component;
use Exception\ComponentInitFailure;

/**
 * Class ComponentChildFailMock
 */
class ComponentChildFailWpErrorMock extends Component {

	/**
	 * @return \Exception\ComponentInitFailure
	 */
	public function setup() {
		return new ComponentInitFailure( 'fail' );
	}
}
