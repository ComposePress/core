<?php

use Abstracts\Component;

/**
 * Class ComponentChildMock
 */
class ComponentReflectionMock extends Component {
	private $child;

	public function __construct( ComponentChildMock $child) {
		$this->child = $child;
	}

	public function test(){
		return $this->child;
	}
}
