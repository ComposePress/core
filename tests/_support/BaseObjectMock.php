<?php

use Abstracts\BaseObject;

/**
 * Class BaseObjectMock
 *
 * @package Abstracts
 * @property string $test
 * @property string $is_test
 */
class BaseObjectMock extends BaseObject {
	private $test = 'test';
	private $is_test = 'test';
	private $get_test = 'test';

	/**
	 * @return mixed
	 */
	public function get_test() {
		return $this->test;
	}

	/**
	 * @param string $test
	 */
	public function set_test( $test ) {
		$this->test = $test;
	}

	public function init() {

	}

	/**
	 * @return string
	 */
	public function is_is_test() {
		return $this->is_test;
	}

	/**
	 * @return string
	 */
	public function get_get_test() {
		return $this->get_test;
	}

}
