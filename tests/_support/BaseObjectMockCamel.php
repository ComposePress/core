<?php

use Abstracts\BaseObject;

/**
 * Class BaseObjectMock
 *
 * @package Abstracts
 * @property string $test
 * @property string $is_test
 */
class BaseObjectMockCamel extends BaseObject {
	private $test = 'test';
	private $is_test = 'test';
	private $get_test = 'test';

	/**
	 * @return mixed
	 */
	public function getTest() {
		return $this->test;
	}

	/**
	 * @param string $test
	 */
	public function setTest( $test ) {
		$this->test = $test;
	}

	public function init() {

	}

	/**
	 * @return string
	 */
	public function isIs_test() {
		return $this->is_test;
	}

	/**
	 * @return string
	 */
	public function getGet_test() {
		return $this->get_test;
	}

}
