<?php


use Abstracts\Component;
use Exception\ComponentInitFailure;

class ComponentFailWpErrorMock extends Component {
	/**
	 * @return \WP_Error
	 */
	public function setup() {
		return new \WP_Error( 'fail' );
	}
}
