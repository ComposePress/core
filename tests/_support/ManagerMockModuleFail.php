<?php


use Abstracts\Manager;

class ManagerMockModuleFail extends Manager {
	protected $modules = [
		'\ComponentChildFailMock',
	];
}
