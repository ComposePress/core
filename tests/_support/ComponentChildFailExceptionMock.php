<?php

use Abstracts\Component;
use Exception\ComponentInitFailure;

/**
 * Class ComponentChildFailMock
 */
class ComponentChildFailExceptionMock extends Component {

	/**
	 * @return \WP_Error
	 */
	public function setup() {
		return new \WP_Error( 'fail' );
	}
}
