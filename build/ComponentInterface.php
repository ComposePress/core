<?php

namespace ComposePress\Core\v0_10_2_0;

/**
 * Interface ComponentInterface
 *
 */
interface ComponentInterface
{
    /**
     * @return bool
     */
    public function setup();
}
/**
 * Interface ComponentInterface
 *
 */
\class_alias('ComposePress\\Core\\v0_10_2_0\\ComponentInterface', 'ComponentInterface', \false);
