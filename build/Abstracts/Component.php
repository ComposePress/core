<?php

namespace ComposePress\Core\v0_10_2_0\Abstracts;

/**
 * Class Component_0_9_0_0
 *
 * @package Abstracts
 * @property \Abstracts\Plugin    $plugin
 * @property \Abstracts\Component $parent
 */
abstract class Component implements \ComposePress\Core\v0_10_2_0\ComponentInterface
{
    use \ComposePress\Core\v0_10_2_0\Traits\Component;
}