<?php

namespace ComposePress\Core\v0_10_2_0\Abstracts;

/**
 * Class BaseObjectAbstract
 *
 * @package Abstracts
 * @property \wpdb       $wpdb
 * @property \WP_Post    $post
 * @property \WP_Rewrite $wp_rewrite
 * @property \WP         $wp
 * @property \WP_Query   $wp_query
 * @property \WP_Query   $wp_the_query
 * @property string      $pagenow
 * @property int         $page
 */
abstract class BaseObject
{
    use \ComposePress\Core\v0_10_2_0\Traits\BaseObject;
}