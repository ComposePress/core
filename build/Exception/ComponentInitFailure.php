<?php

namespace ComposePress\Core\v0_10_2_0\Exception;

/**
 * Class ComponentInitFailure
 *
 * @package Exception
 */
class ComponentInitFailure extends \Exception
{
}
