<?php

namespace ComposePress\Core\v0_10_2_0\Exception;

/**
 * Class ContainerInvalid
 *
 * @package Exception
 */
class ContainerInvalid extends \Exception
{
}
