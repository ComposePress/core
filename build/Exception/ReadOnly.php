<?php

namespace ComposePress\Core\v0_10_2_0\Exception;

/**
 * Class ReadOnly
 *
 * @package Exception
 */
class ReadOnly extends \Exception
{
}
