<?php

namespace ComposePress\Core\v0_10_2_0\Exception;

/**
 * Class InexistentProperty
 *
 * @package Exception
 */
class InexistentProperty extends \Exception
{
}
