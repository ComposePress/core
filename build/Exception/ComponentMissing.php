<?php

namespace ComposePress\Core\v0_10_2_0\Exception;

/**
 * Class ComponentMissing
 *
 * @package Exception
 */
class ComponentMissing extends \Exception
{
}
