<?php

namespace ComposePress\Core\v0_10_2_0\Exception;

/**
 * Class ContainerNotExists
 *
 * @package Exception
 */
class ContainerNotExists extends \Exception
{
}
