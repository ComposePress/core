<?php

namespace Abstracts;

/**
 * Class Component_0_9_0_0
 *
 * @package Abstracts
 * @property \Abstracts\Plugin    $plugin
 * @property \Abstracts\Component $parent
 */
abstract class Component implements \ComponentInterface {
	use \Traits\Component;
}
