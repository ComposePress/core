<?php
/**
 * Interface ComponentInterface
 *
 */
interface ComponentInterface {
	/**
	 * @return bool
	 */
	public function setup();
}
