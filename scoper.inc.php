<?php

declare( strict_types=1 );

use Isolated\Symfony\Component\Finder\Finder;

/** @var Symfony\Component\Finder\Finder $finder */

if ( class_exists( 'Isolated\Symfony\Component\Finder\Finder' ) ) {
	$finder = 'Isolated\Symfony\Component\Finder\Finder';
} else {
	$finder = 'Symfony\Component\Finder\Finder';
}

return [
	'finders'   => [
		$finder::create()->files()->in( 'src' ),
	],
	'whitelist' => [
		'ComposePress\Dice\*',
	],
	'patchers'  => [
		function ( $filePath, $prefix, $content ) {
			if ( 'Hooker.php' === basename( $filePath ) ) {
				foreach ( [ 'add_filter', 'add_action', 'remove_filter', 'remove_action' ] as $func ) {
					$content = str_replace( "{$prefix}\\{$func}", $func, $content );
				}
			}

			return $content;
		},
	],
];
